#!/bin/sh

# coverage: clean, build, deploy
rm -rf htmlcov
sudo rm -rf /var/www/html/PyOTRS/htmlcov
/usr/local/bin/coverage html -d htmlcov
sudo cp -a htmlcov /var/www/html/PyOTRS/

# docs: clean, build, deploy
rm -rf build/docs
sudo rm -rf /var/www/html/PyOTRS/docs
tox -e docs
sudo cp -a build/docs /var/www/html/PyOTRS/docs

# check format (README)
python setup.py check --strict --metadata --restructuredtext
